require 'fileutils'

module ConvertToJson
  def self.process(site, payload)
    site.collections['websites'].docs.each do |x|
      f = x.destination("")
      FileUtils.mv f, "#{File.dirname(f)}/#{File.basename(f,'.*')}.json"
    end
  end
end

Jekyll::Hooks.register :site, :post_write do |site, payload|
  ConvertToJson.process(site, payload)
end