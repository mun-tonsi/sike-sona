---
title: Home
layout: default
---

# Welcome to {{ site.title }}!
{{ site.description }}

## People
{% for site in site.websites %}
1.  [{{ site.title }}]({{ site.link }}): {{ site.description }}
{% endfor %}