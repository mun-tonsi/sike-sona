---
title: About
layout: default
permalink: /about
---

# About
This is a web ring, a relatively early invention in web technology. It is a list of websites arranged into a ring, which allows visitors of a site to explore similar websites to the one they are currently visiting. You are currently on the index of the ring, a place to view all websites contained within it.

This software is called _sike_, which means "circle" or "ring" in Toki Pona. _sona_ means "knowledge" in Toki Pona, as this is a ring of knowledge, so to speak! Folks across computational, social sciences, arts, and humanities disciplines are welcome to join this web ring, as long as their work focuses on either: applying computational methods to work on social sciences, arts, or humanities-based areas, or using social science, arts, or humanities-based approaches to investigate areas in computing (there's a nice symmetry there). If you think your work fits this, you're welcome to add your website here!

You can either submit a pull request adding a Markdown file of the following format to the `_websites` directory in the GitLab repository. If you're unsure, email [Genevieve Clifford](mailto:genevieve@mun-tonsi.net) with your name, a 1-2 sentence bio (look at the other folks on the main page for examples), and a link to the page you'd like to include.

```md
---
title: Genevieve Clifford
link: https://cs.swansea.ac.uk/~gcli
description: Social Computer Science PhD Student at Swansea University's [FITLab](https://fitlab.eu). In her PhD, she is exploring transgender digital poverty in Wales.
---
```

When the pull request is merged (I'll let you know if you aren't able to do this yourself), a JSON file will be generated for you to do what you'd like with, as well as including your information on this site's index. The JSON file will be located at [sike-sona.mun-tonsi.net/sites/yoursite.json](https://sike-sona.mun-tonsi.net/yoursite.json) (replace `yoursite` with whatever the filename of the Markdown file added to the `_websites` directory is, or you will get this in an email if you're not doing this self-service). It will look a little like this:

```json
{
  "next": {
    "link": "https://joebloggs.example.com",
    "title": "Joe Bloggs",
    "description": "Lecturer in Sociology at Example University, their work focuses on [some sociological areas]"
  },
  "prev": {
    "link": "https://cs.swansea.ac.uk/~gcli",
    "title": "Genevieve Clifford",
    "description": "Social Computer Science PhD Student at Swansea University's [FITLab](https://fitlab.eu). In her PhD, she is exploring transgender digital poverty in Wales."
  },
  "index": {
    "link": "https://sike-sona.mun-tonsi.net",
    "title": "sike sona",
    "description": "sike sona is a web ring for folks doing work combining computing and social research"
  }
}
```
Feel free to implement whatever method you'd like to use this JSON, which will update whenever changes are made to the web ring (you don't have to do anything when it changes). I have written the following snippet of HTML + JS which you can add to the footer of your website to display your position in the web ring, and its index (just make sure you change `yoursite` as before); theme using CSS to your heart's content!

```html
<footer>
  <p>
    Something here!
  </p>
  <div id="sike" hidden>
    <a id="index"></a>
    <a id="prev">(←) </a>
    <a id="next">(→) </a>
  </div>
</footer>

<script>
  const elements = ["index", "prev", "next"]
  const sike = document.getElementById('sike');

  fetch('https://sike-sona.mun-tonsi.net/sites/yoursite.json')
      .then(res => res.json())
      .then((out) => {
        sike.hidden = false;
        elements.forEach((element) => {
          var a = document.getElementById(element);
          var link = document.createTextNode(out[element]['title']);
          a.appendChild(link);
          a.title += out[element]['title'];
          a.href = out[element]['link'];
          sike.appendChild(a);
        });
      });
</script>
```

Please feel free to [email me](mailto:genevieve@mun-tonsi.net) if you have any questions about any of this!