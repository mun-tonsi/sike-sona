---
layout: default
title: Attributions
---

## Atkinson Hyperlegible
Used under the terms of the [Atkinson Hyperlegible Font License](https://brailleinstitute.org/wp-content/uploads/2020/11/Atkinson-Hyperlegible-Font-License-2020-1104.pdf) with the following copyright attribution. Copyright © 2020, Braille Institute of America, Inc., [https://www.brailleinstitute.org/freefont](https://www.brailleinstitute.org/freefont) with Reserved Typeface Name Atkinson Hyperlegible Font.

## MIT Licence
Jekyll static site generator: Copyright © 2008-present Tom Preston-Werner and Jeykll contributors

Solarized colour themes: Copyright © 2011 Ethan Schoonover

Dracula colour theme: Copyright © 2023 [Zeno Rocha](mailto:hi@zenorocha.com)

```
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```