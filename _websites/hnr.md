---
title: honor ash
link: https://hnr.fyi
description: Artist and writer based in Norwich, UK; their work explores communication, community, trust & power, both on and offline.
---