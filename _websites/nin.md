---
title: Ninian James Frenguelli
link: https://ninianjames.github.io
description: Criminology PhD Student in [CYTREC](https://www.swansea.ac.uk/law/cytrec/) at Swansea University. His PhD uses computational methods (scraping, analysis) to explore gender in online spaces used by the far right. Ninian is part of the [Welsh Graduate School for Social Sciences](https://walesdtp.ac.uk).
---