---
title: Genevieve Clifford
link: https://tomo.mun-tonsi.net
description: Computer Science (Digital Economy & Society) PhD Student at Swansea University's [FITLab](https://fitlab.eu). In her PhD, she is exploring transgender digital poverty in Wales. Genevieve is part of the [Welsh Graduate School for Social Sciences](https://walesdtp.ac.uk).
---